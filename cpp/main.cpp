#include <iostream>
#include <fstream>
#include <exception>

#include "PcapFile.h"

int main(int argc, char **argv)
{
	if(argc <= 0)
	{
		std::cout << "No input file.\n";
		exit(0);
	}
	PcapFile pcap;
	try
	{
		pcap.parseFile(argv[1]);
	}
	catch(std::exception &e)
	{
		std::cout << "No file specified.\n"; 
		exit(-2);
	}
	pcap.showGlobalHeaderInfos();
	return 0;
}
