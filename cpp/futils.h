#ifndef FUTILS_H
#define FUTILS_H

#include <fstream>
#include <exception>
#include <vector>

#define BIGENDIAN		1
#define LITTLEENDIAN	2
#define MIDENDIAN		3	// not fully supported

typedef unsigned int	uint32;
typedef signed   int	int32;
typedef unsigned short	uint16;
typedef signed   short	int16;
typedef unsigned char	uint8;
typedef signed   char	int8;

uint32 read_uint32(std::ifstream &file, int endianness = BIGENDIAN);
 int32 read_int32 (std::ifstream &file, int endianness = BIGENDIAN);
uint16 read_uint16(std::ifstream &file, int endianness = BIGENDIAN);
 int16 read_int16 (std::ifstream &file, int endianness = BIGENDIAN);
 uint8 read_uint8 (std::ifstream &file, int endianness = BIGENDIAN);
  int8 read_int8  (std::ifstream &file, int endianness = BIGENDIAN);

struct PcapFileHeader
{
	uint32 magicNumber;
	uint16 versionMajor;
	uint16 versionMinor;
	 int32 timeCorrection;
	uint32 timestampsAccuracy;
	uint32 maxPacketLength;
	uint32 dataLinkType;

	int endianness;
};

struct PcapPacket
{
	uint32 timestampInSeconds;
	uint32 timestampInMicroseconds;
	uint32 numberBytesSaved;
	uint32 numberBytesOriginal;
	int ID;
	std::vector<uint8> data;
};

#endif // FUTILS_H
