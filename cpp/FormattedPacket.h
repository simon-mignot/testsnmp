#ifndef FORMATTEDPACKET_H
#define FORMATTEDPACKET_H

#include <iostream>
#include "FrameDecode.h"
#include "futils.h"


#define FORMAT_BASICINFOS			1
#define FORMAT_EXTENDEDINFOS		2
#define FORMAT_HEXDATA				4
#define FORMAT_ASCIIDATA			8
#define FORMAT_DECODE				16
#define FORMAT_HIGHLIGHT_DECODE		32
#define FORMAT_NETBIOS_RESOLUTION	64
#define FORMAT_HOSTNAME_RESOLUTION	128
#define FORMAT_MANUFACTURER			256
#define OUTPUTFORMAT_TEXT			512
#define OUTPUTFORMAT_HTML			1024

class FormattedPacket
{
	public:
		FormattedPacket();
		FormattedPacket(PcapPacket packet);
		virtual ~FormattedPacket();

		void setPacket(PcapPacket packet);

		void setOptions(uint32 flags);
		void unsetOptions(uint32 flags);

		void displayPacket();

		bool isSet(int flag);

	private:
		void displayBasicsInfos();
		void displayExtendedInfos();
		void displayData();
			uint32 displayHex(uint32 hex);
			void displayASCIILine(std::vector<char> data);
		void displayDecode();


		void showColumnsID();




		uint32 m_flags;
		PcapPacket m_packet;
		
};

#endif // FORMATTEDPACKET_H
