#ifndef PCAPFILE_H
#define PCAPFILE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include "futils.h"
#include "FormattedPacket.h"

class PcapFile
{
	public:
		PcapFile();
		virtual ~PcapFile();

		void parseFile(std::string filename);
		void readPackets(int number = -1); // -1 = all


		void showGlobalHeaderInfos();
		void showPackets(int begin = 0, int end = -1);
		void showPacketInfos(PcapPacket packet, int packetID);
		void showFormattedPacket(std::vector<uint8> data);
		void showReadableData(std::vector<char> data);
		void showColumnsID();

		int getEndianness();

		std::string getDataLinkTypeName();

	private:
		PcapFileHeader m_header;
		std::vector<PcapPacket> m_packets;
		std::ifstream m_pcapfile;

};


#endif // PCAPFILE_H



