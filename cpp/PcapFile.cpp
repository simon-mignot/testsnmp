#include "PcapFile.h"

PcapFile::PcapFile()
{
	
}

PcapFile::~PcapFile()
{

}

void PcapFile::parseFile(std::string filename)
{
	m_pcapfile.open(filename.c_str(), std::ios::in | std::ios::binary);
	if(!m_pcapfile)
	{
		std::cout << "Error opening file " << filename << ".\n";
		exit(-1);
	}
	m_header.magicNumber		= read_uint32(m_pcapfile);
	m_header.endianness			= this->getEndianness();
	m_header.versionMajor		= read_uint16(m_pcapfile, m_header.endianness);
	m_header.versionMinor		= read_uint16(m_pcapfile, m_header.endianness);
	m_header.timeCorrection		= read_int32(m_pcapfile, m_header.endianness);
	m_header.timestampsAccuracy	= read_uint32(m_pcapfile, m_header.endianness);
	m_header.maxPacketLength	= read_uint32(m_pcapfile, m_header.endianness);
	m_header.dataLinkType		= read_uint32(m_pcapfile, m_header.endianness);
	this->readPackets(-1);
}

void PcapFile::readPackets(int number)
{
	try
	{
		for(unsigned int i = 0; i < (unsigned int)number; ++i)
		{

			PcapPacket tempPacket;
			tempPacket.timestampInSeconds 		= read_uint32(m_pcapfile, m_header.endianness);
			tempPacket.timestampInMicroseconds 	= read_uint32(m_pcapfile, m_header.endianness);
			tempPacket.numberBytesSaved			= read_uint32(m_pcapfile, m_header.endianness);
			tempPacket.numberBytesOriginal		= read_uint32(m_pcapfile, m_header.endianness);
			for(int j = 0; j < tempPacket.numberBytesSaved; ++j)
				tempPacket.data.push_back(read_uint8(m_pcapfile, m_header.endianness));
			m_packets.push_back(tempPacket);
		}
	}
	catch(std::exception &e)
	{
		std::cout << "exception catched\n";
	}
	std::cout << "out of try/catch\n";
	this->showPackets(0, number);
}

// Show formatted packets
void PcapFile::showPackets(int begin, int end)
{
	for(unsigned int i = begin; i < (unsigned int)end && i < m_packets.size(); ++i)
	{
		FormattedPacket packet(m_packets[i]);
		packet.setOptions(FORMAT_BASICINFOS | FORMAT_EXTENDEDINFOS | FORMAT_HEXDATA | FORMAT_ASCIIDATA);
		packet.displayPacket();
	}
}

void PcapFile::showPacketInfos(PcapPacket packet, int packetID)
{
	std::string days[7] = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};
	std::string months[12] = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"};
	time_t t = packet.timestampInSeconds;
	tm time = *localtime(&t);
	std::cout.width(2);
	std::cout.fill('0');
	std::cout << std::dec << "\nPacket #" << packetID << " - Date : " 
		  << days[time.tm_wday] << " " << time.tm_mday << " " << months[time.tm_mon] << " " << time.tm_year + 1900
		  << " à " << time.tm_hour << ":" << time.tm_min << ":" << time.tm_sec << "." << packet.timestampInMicroseconds << "\n";
}

void PcapFile::showGlobalHeaderInfos()
{
	std::string endian[3] = {"big-endan", "little-endian", "mid-endian"};
	std::cout << std::hex << "Magic Number : " << m_header.magicNumber << " - " << endian[1] << '\n';
	std::cout << std::hex << "Version : " << m_header.versionMajor << "." << m_header.versionMinor << '\n';
	std::cout << std::dec << "Time correction : " << m_header.timeCorrection << '\n';
	std::cout << std::dec << "Timestamps accuracy : " << m_header.timestampsAccuracy << '\n';
	std::cout << std::dec << "Max packet length : " << m_header.maxPacketLength << '\n';
	std::cout << std::hex << "Data link type : " << m_header.dataLinkType <<  " - " << this->getDataLinkTypeName() << '\n';

}

int PcapFile::getEndianness()
{
	if(m_header.magicNumber == 0xa1b2c3d4)
		return BIGENDIAN;
	else if(m_header.magicNumber == 0xd4c3b2a1)
		return LITTLEENDIAN;
	else
		return MIDENDIAN;
}


std::string PcapFile::getDataLinkTypeName()
{
	std::ifstream file("linktype.data");
	while(file.good())
	{
		std::string name;
		int id;
		file >> id >> name;
		if(id == m_header.dataLinkType)
			return name;
	}
	return "UNKNOW";
}
