#ifndef FRAMEDECODE_H
#define FRAMEDECODE_H

#include <iostream>
#include <vector>
#include "futils.h"
#include "Protocols.h"

class FrameDecode
{
	public:
		FrameDecode();
		FrameDecode(std::vector<uint8> frame);
		virtual ~FrameDecode();
		
		void display();

	private:
		std::vector<uint8> m_data;	
};

#endif // FRAMEDECODE_H
