#include "futils.h"

uint32 read_uint32(std::ifstream &file, int endianness)
{
	uint32 ret = 0;
	for(int i = 0; i < 4; ++i)
	{
		if(!file.good())
		{
			throw std::exception();
		}
		ret |= file.get();
		if(i < 3)
			ret <<= 8;
	}
	if(endianness == BIGENDIAN)
		return ret;
	else if(endianness == LITTLEENDIAN)
		return (ret >> 24) | ((ret << 8) & 0x00FF0000) | ((ret >> 8) & 0x0000FF00) | (ret << 24);
	else if(endianness == MIDENDIAN)
		return ret;
}
int32 read_int32(std::ifstream &file, int endianness)
{
	return (int32)read_uint32(file, endianness);
}
uint16 read_uint16(std::ifstream &file, int endianness)
{
	uint16 ret = 0;
	for(int i = 0; i < 2; ++i)
	{
		if(!file.good())
			throw std::exception();
		ret |= file.get();
		if(i < 1)
			ret <<= 8;
	}
	if(endianness == BIGENDIAN)
		return ret;
	else if(endianness == LITTLEENDIAN)
		return (ret >> 8) | (ret << 8);
	else if(endianness == MIDENDIAN)
		return ret;
}
int16 read_int16(std::ifstream &file, int endianness)
{
	return (int16)read_uint16(file, endianness);
}
uint8 read_uint8(std::ifstream &file, int endianness)
{
	uint8 ret = 0;
	if(!file.good())
		throw std::exception();
	ret |= file.get();
	return ret;
}
int8 read_int8(std::ifstream &file, int endianness)
{
	return (int8)read_uint8(file, endianness);
}

