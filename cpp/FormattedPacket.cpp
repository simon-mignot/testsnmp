#include "FormattedPacket.h"

FormattedPacket::FormattedPacket()
{

}

FormattedPacket::FormattedPacket(PcapPacket packet) : m_packet(packet)
{

}

FormattedPacket::~FormattedPacket()
{

}

void FormattedPacket::setOptions(uint32 flags)
{
	m_flags |= flags;
}
void FormattedPacket::unsetOptions(uint32 flags)
{
	m_flags &= ~flags;
}


void FormattedPacket::displayPacket()
{
	if(isSet(FORMAT_BASICINFOS))
		this->displayBasicsInfos();
	if(isSet(FORMAT_EXTENDEDINFOS))
		this->displayExtendedInfos();
	this->displayData();
	if(isSet(FORMAT_DECODE))
		this->displayDecode();
	std::cout << "\n=================----\n";
}

void FormattedPacket::displayBasicsInfos()
{
	std::string days[7] = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};
	std::string months[12] = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"};
	time_t t = m_packet.timestampInSeconds;
	tm time = *localtime(&t);
	std::cout.width(2);
	std::cout.fill('0');
	std::cout << std::dec << "\nPacket #" << m_packet.ID << " - Date : " 
		  << days[time.tm_wday] << " " << time.tm_mday << " " << months[time.tm_mon] << " " << time.tm_year + 1900
		  << " à " << time.tm_hour << ":" << time.tm_min << ":" << time.tm_sec << "." << m_packet.timestampInMicroseconds << "\n";
}
void FormattedPacket::displayExtendedInfos()
{
	std::cout << "Taille capturé  : " << m_packet.numberBytesSaved << " bytes (" 	  << m_packet.numberBytesSaved    * 8 << " bits)\n"
		      << "Taille original : " << m_packet.numberBytesOriginal << " bytes (" << m_packet.numberBytesOriginal * 8 << " bits)\n"
			  << "Capturé : " << (float)m_packet.numberBytesSaved / (float)m_packet.numberBytesOriginal *(float) 100.0 << '%' << '\n';
}
void FormattedPacket::displayData()
{
	std::vector<char> char_data;

	this->showColumnsID();
	for(int i = 0; i < m_packet.data.size(); ++i)
	{
		if(i%16 == 0)
		{
			this->displayASCIILine(char_data);
			std::cout.width(0); std::cout << '\n'; std::cout.width(8); std::cout.fill('0');
			std::cout << i << "\t";
			char_data.clear();
		}
		else if(i%8 == 0)
		{
			std::cout << " ";
		}
		char_data.push_back(this->displayHex(m_packet.data[i]));
	}
	if(isSet(FORMAT_ASCIIDATA | FORMAT_HEXDATA))
	{
		int padding = (16 - char_data.size());
		for(int i = 0; i < padding; ++i)
			std::cout << "   ";
	}
	this->displayASCIILine(char_data);
	std::cout << "\n--\n";
}

void FormattedPacket::showColumnsID()
{
	bool hex = isSet(FORMAT_HEXDATA);
	std::cout << "        \t";
	for(int j = 0; j < 16; ++j)
	{
		if(j%8 == 0 && hex)
			std::cout << " ";
		std::cout << std::hex << j;
		if(hex)
			std::cout << "  ";
	}
}

uint32 FormattedPacket::displayHex(uint32 hex)
{
	if(this->isSet(FORMAT_HEXDATA))
	{
		std::cout.width(2); std::cout.fill('0');
		std::cout << std::hex << hex << std::dec << ' ';
	}
	return hex;
}

void FormattedPacket::displayASCIILine(std::vector<char> data)
{
	if(this->isSet(FORMAT_ASCIIDATA))
	{
		std::cout << '\t';
		for(int i = 0; i < data.size(); ++i)
		{
			if(isprint(data[i]))
				std::cout << data[i];
			else
				std::cout << '.';
		}
	}
}

void FormattedPacket::displayDecode()
{
	FrameDecode decoder(m_packet.data);
	decoder.display();
}

bool FormattedPacket::isSet(int flag)
{
	return m_flags & flag == flag;
}





