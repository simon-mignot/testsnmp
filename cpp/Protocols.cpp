#include "Protocols.h"

Ethernet::Ethernet(Data data)
{
	std::cout << "\n\n-= Trame Ethernet =-\n";
	std::cout << "Destination MAC : ";
	for(int i = 0; i < 6; ++i)
	{
		std::cout.width(2);
		std::cout.fill('0');
		std::cout << std::hex << (uint32)data[i];
		if(i < 5)
			std::cout << ':';
	}
	std::cout << std::dec << "\nSource MAC : ";
	for(int i = 0; i < 6; ++i)
	{
		std::cout.width(2);
		std::cout.fill('0');
		std::cout << std::hex << (uint32)data[i+6];
		if(i < 5)
			std::cout << ':';
	}
	std::cout << std::dec << "\nEthernet type : ";
	int ethtype = (data[12] << 8) | data[13];
	std::cout << "0x";
	std::cout.width(4);
	std::cout << std::hex << ethtype;
	std::cout.width(0);
	std::cout << std::dec;	
	
	if(ethtype == 0x0800)
	{
		Data nextData = Data(data.begin() + 14, data.end());
		IPv4 ipv4(nextData);
	}
}
Ethernet::~Ethernet()
{

}

IPv4::IPv4(Data data)
{
	std::string protocol;
	if(data[9] == 6)
		protocol = "TCP";
	else if(data[9] == 17)
		protocol = "UDP";
	else if(data[9] == 1)
		protocol = "ICMP";
	else
		protocol = "Unknow";

	std::cout << "\n\n-= Protocol IP =-\n";
	std::cout << "Version d'IP : "							<< (uint32)(data[0] >> 4)								<< '\n';
	std::cout << "Longueur de l'en-tête : "					<< (uint32)((data[0] << 4) >> 4)						<< '\n';
	std::cout << "Type de service : "						<< (uint32)(data[1])									<< '\n';	// TODO	type de service
	std::cout << "Longueur du packet IPv4 : "				<< (uint32)(((uint32)data[2] << 8) | data[3])			<< '\n';
	std::cout << "Numero d'identification du paquet : "		<< (uint32)(((uint32)data[4] << 8) | data[5])			<< '\n';
	std::cout << "Indicateurs : "							<< (uint32)(data[6] >> 5)								<< '\n';
	std::cout << "Fragment offset : "						<< (uint32)(((data[6] << 3) >> 3) | data[7])			<< '\n';
	std::cout << "Durée de vie : "							<< (uint32)(data[8])									<< '\n';
	std::cout << "Protocole de niveau supérieur : "			<< protocol << '(' << (uint32)(data[9]) << ')'			<< '\n';
	std::cout << "Somme de controle : 0x" << std::hex		<< (uint32)((uint32)(data[10] << 8) | data[11])			<< '\n';

	std::cout << std::dec;
	std::cout << "Address IP source : " << (uint32)data[12] << '.' << (uint32)data[13] << '.' 
										<< (uint32)data[14] << '.' << (uint32)data[15] << '\n';

	std::cout << "Address IP Destination : " 	<< (uint32)data[16] << '.' << (uint32)data[17] << '.'
												<< (uint32)data[18] << '.' << (uint32)data[19]	<< '\n';

	std::cout << "Options et remplissage : " << std::hex 
			<< (((uint32)data[20] << 24) | ((uint32)data[21] << 16) | ((uint32)data[22] << 8) | (uint32)data[23]) << '\n';

	if(data[9] == 17)
	{
		Data nextData = Data(data.begin() + 20, data.end());
		UDP udp(nextData);
	}
	
}
IPv4::~IPv4()
{

}

UDP::UDP(Data data)
{
	uint32 source = (uint32)(((uint32)data[0] << 8) | (uint32)data[1]);
	uint32 destination = (uint32)(((uint32)data[2] << 8) | (uint32)data[3]);
	std::cout << std::dec;
	std::cout << "\n\n-= Protocole UDP =-\n";
	std::cout << "Port source : " << source << '\n';
	std::cout << "Port destination : " << destination << '\n';
	std::cout << "Longueur du packet UDP : " << (((uint32)data[4] << 8) | (uint32)data[5]) << '\n';
	std::cout << "Somme de controle : 0x" << std::hex << (((uint32)data[6] << 8) | (uint32)data[7]) << '\n';
	
	if(destination == 161 || destination == 162 || source == 161 || source == 162)
	{
		Data nextData = Data(data.begin() + 8, data.end());
		SNMP snmp(nextData);
	}
}
UDP::~UDP()
{

}

SNMP::SNMP(Data data)
{
	ASN1Value asn1value;
	asn1value.end = false;
	asn1value.nextData = data.begin();
	std::cout << "\n\n-= Protocole SNMP =-\n";
	std::cout << std::dec << '\n';

	while(!asn1value.end)
		asn1value = ASN1::getValue(asn1value.nextData,data.end());
	/*//if(asn1value.type == 0x30)
	//{
		asn1value = ASN1::getValue(asn1value.nextData, data.end());
		//if(asn1value.type == 0x02)
		std::cout << "Version : " << std::hex << (uint32)asn1value.stringValue[0] << " - " << (uint32)asn1value.stringValue[1] << std::dec << '\n';
		asn1value = ASN1::getValue(asn1value.nextData, data.end());
		//if(asn1value.type == 0x04)
			std::cout << "Communauté : " << asn1value.stringValue << '\n';
	//}
	asn1value = ASN1::getValue(asn1value.nextData, data.end());
	//if(asn1value.type == 0x30)
	//{
		asn1value = ASN1::getValue(asn1value.nextData, data.end());
	//	if(asn1value.type == 0x02)
			std::cout << "Requête ID : " << asn1value.stringValue << '\n';
		asn1value = ASN1::getValue(asn1value.nextData, data.end());
	//	if(asn1value.type == 0x02)
			std::cout << "Statut d'erreur : " << asn1value.stringValue << '\n';
		asn1value = ASN1::getValue(asn1value.nextData, data.end());
	//	if(asn1value.type == 0x02)
			std::cout << "Index d'erreur : " << asn1value.stringValue << '\n';

	//}
	asn1value = ASN1::getValue(asn1value.nextData, data.end());
//	if(asn1value.type == 0x30)
//	{
		asn1value = ASN1::getValue(asn1value.nextData, data.end());
//		if(asn1value.type == 0x30)
//		{
//			if(asn1value.type == 0x06)
				std::cout << "OID : " << asn1value.stringValue << '\n';
//		}
//	}*/
}
SNMP::~SNMP()
{

}

ASN1::ASN1()
{

}
ASN1::~ASN1()
{

}
ASN1Value ASN1::getValue(Data::iterator begin, Data::iterator end)
{
	Data::iterator iterator = begin;
	ASN1Value ret;
	ret.end = false;
	ret.type = *iterator;
	ret.length = *(iterator + 1);
	{
		Data::iterator it = iterator + 2;
		int i = 0;
//		std::cout << "DUMP: length : " << ret.length << "\n";
		for(it, i; it != end && i < ret.length; ++i, ++it)
		{
			ret.value.push_back(*it);
//			std::cout << std::hex << (uint32)*it << std::dec << " ";
		}
		ret.end = it == end && ret.type != ASN1_SEQUENCE && !(isContext(ret.type));
//		std::cout << "\nEND DUMP\n";
	}
	ret.nextData = ret.type == ASN1_SEQUENCE || isContext(ret.type) ? begin + 2 : begin + ret.length + 2;
/*	std::cout << "\n\n\nDATA DUMP\n";
	for(Data::iterator it = begin; it != end; ++it)
		std::cout << std::hex << (uint32)*it << " ";
	std::cout << std::dec << '\n'; 
	for(Data::iterator it = begin; it != end; ++it)
		if(isprint(*it))
			std::cout << *it;
		else
			std::cout << '.';
	
	std::cout << "\nNEXT DATA DUMP\n";
	for(Data::iterator it = ret.nextData; it != ret.nextData + ret.length; ++it)
		std::cout << std::hex << (uint32)*it << " ";
	std::cout << std::dec << '\n';
	for(Data::iterator it = ret.nextData; it != ret.nextData + ret.length; ++it)
		if(isprint(*it))
			std::cout << *it;
		else
			std::cout << '.';
	std::cout << '\n';*/
	
	for(int i = 0; i < ret.value.size(); ++i)
		if(isprint(ret.value[i]))
		{
			if(ret.type == ASN1_OBJECT_IDENTIFIER)
			{
				ret.stringValue+='.';
				ret.stringValue+=(uint32)ret.value[i];
			}
			else
				ret.stringValue+=(char)ret.value[i];
		}
		else
			ret.stringValue+='.';
	switch(ret.type)
	{
		// Primitive ASN1 type
		case ASN1_INTEGER : ret.stringType = "integer"; break;
		case ASN1_BIT_STRING : ret.stringType = "bit string"; break;
		case ASN1_STRING : ret.stringType = "string"; break;
		case ASN1_NULL : ret.stringType = "null"; break;
		case ASN1_OBJECT_IDENTIFIER : ret.stringType = "object identifier"; break;
		
		// Constructed ASN1 type
		case ASN1_SEQUENCE : ret.stringType = "sequence"; break;
		
		// Primitive SNMP
		case ASN1_IP_ADDRESS : ret.stringType = "IP Address"; break;
		case ASN1_COUNTER32 : ret.stringType = "Counter32"; break;
		case ASN1_GAUGE32 : ret.stringType = "Gauge32"; break;
		case ASN1_TIME_TICKS : ret.stringType = "Time ticks"; break;
		case ASN1_OPAQUE : ret.stringType = "Opaque"; break;
		case ASN1_NSAP_ADDRESS : ret.stringType = "Nsap Address"; break;
		case ASN1_COUNTER64 : ret.stringType = "Counter64"; break;
		case ASN1_UINT32 : ret.stringType = "Unsigned integer32"; break;

		// SNMP context code
		case ASN1_GET_REQUEST : ret.stringType = "GetRequestPDU"; break;
		case ASN1_GET_NEXT_REQUEST : ret.stringType = "GetNextRequestPDU"; break;
		case ASN1_GET_RESPONSE : ret.stringType = "GetResponsePDU"; break;
		case ASN1_SET_REQUEST : ret.stringType = "SetRequestPDU"; break;
		case ASN1_TRAP : ret.stringType = "TrapPDU"; break;
		case ASN1_GET_BULK_REQUEST : ret.stringType = "GetBulkRequestPDU"; break;
		case ASN1_INFORM_REQUEST : ret.stringType = "InformRequestPDU"; break;
		case ASN1_TRAP_V2 : ret.stringType = "TrapPDU-v2"; break;
		default:
			ret.stringType = "Unknow";
			break;
	}
	
	std::cout << "ret.type : " << std::hex << ret.type << std::dec << " (" << ret.stringType << ")\n";
	std::cout << "ret.length : " << ret.length << std::hex << "(0x" << ret.length << ")" << std::dec <<'\n';
	std::cout << "ret.value : " << ret.stringValue << '\n' << '\n';
	return ret;
}


