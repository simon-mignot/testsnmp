#ifndef PROTOCOLS_H
#define PROTOCOLS_H

#include <iostream>
#include <vector>
#include "futils.h"

#define ASN1_INTEGER			0x02
#define ASN1_BIT_STRING			0x03
#define ASN1_STRING				0x04
#define ASN1_NULL				0x05
#define ASN1_OBJECT_IDENTIFIER	0x06

#define ASN1_SEQUENCE			0x30

#define ASN1_IP_ADDRESS			0x40
#define ASN1_COUNTER32			0x41
#define ASN1_GAUGE32			0x42
#define ASN1_TIME_TICKS			0x43
#define ASN1_OPAQUE				0x44
#define ASN1_NSAP_ADDRESS		0x45
#define ASN1_COUNTER64			0x46
#define ASN1_UINT32				0x47

#define ASN1_GET_REQUEST		0xA0
#define ASN1_GET_NEXT_REQUEST	0xA1
#define ASN1_GET_RESPONSE		0xA2
#define ASN1_SET_REQUEST		0xA3
#define ASN1_TRAP				0xA4
#define ASN1_GET_BULK_REQUEST	0xA5
#define ASN1_INFORM_REQUEST		0xA6
#define ASN1_TRAP_V2			0xA7

#define isContext(var)	(var >> 4) == 0xA

typedef std::vector<uint8> Data;

class Ethernet
{
	public:
		Ethernet(Data data);
		virtual ~Ethernet();
};

class IPv4
{
	public:
		IPv4(Data data);
		virtual ~IPv4();
};

class UDP
{
	public:
		UDP(Data data);
		virtual ~UDP();
};

class SNMP
{
	public:
		SNMP(Data data);
		virtual ~SNMP();
};
struct ASN1Value
{
	int type;
	int length;
	Data value;
	Data::iterator nextData;

	bool end;

	std::string stringType;
	std::string stringValue;
};

class ASN1
{
	public:
		ASN1();
		virtual ~ASN1();
		static ASN1Value getValue(Data::iterator begin, Data::iterator end);
};


#endif // PROTOCOLS_H
