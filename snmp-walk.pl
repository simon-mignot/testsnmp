#!/usr/bin/perl -w

#===============================================================================
# Auteur : Simon Mignot - simon.mignot.lasalle@gmail.com
# Date   : 29/01/2014 - 8:11:32
# Site   : https://bitbucket.org/simon-mignot/testsnmp
#===============================================================================

use Net::SNMP;

sub Exit
{
	my ($message) = @_;
	print "\n======\n$message\n======\n";
}
sub sig_caught
{
	$sig = 1;
}

$sig = 0;
$SIG{'INT'} = \&sig_caught;
$hostip = '10.1.10.250';
$community = 'cove';
$numberResult = 100000;

($snmp, $error) = Net::SNMP->session(
	-hostname	=> $hostip,
	-community	=> $community,
	-version	=> 1,
	-timeout	=> 5
	);
if(!defined($snmp))
{
	print $error;
	exit -1;
}

$baseOID = $ARGV[0];
$currentOID = $baseOID;
$currentResult = 0;
$nextsBranches = 0;

while(1)
{
	$snmp->get_next_request($currentOID);
	$currentOID = ($snmp->var_bind_names())[0];
	if(!Net::SNMP::oid_base_match($baseOID, $currentOID) && !$nextsBranches)
	{
		Exit("Next OID is out of the current branch.\n$currentResult displayed."); # Add an option for continue on next branches
		last;
	}
	if(!defined($currentOID))
	{
		Exit("OID doesn't exist.\n$currentResult displayed.");
		last;
	}
	print "$currentOID - " . $snmp->get_request($currentOID)->{$currentOID} . "\n";
	$currentResult++;
	if($currentResult == $numberResult || $sig == 1)
	{
		Exit("$currentResult displayed.");
		last;
	}
}
